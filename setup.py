from setuptools import find_packages, setup

setup(
    name="fts",
    version="0.1.0",
    packages=find_packages(),
    description="Featuretools on Spark.",
    license="MIT",
    url="https://gitlab.com/brilhana/ftsp",
    author="Alexandre Brilhante",
    author_email="alexandre.brilhante@gmail.com",
    install_requires=open("requirements.txt").readlines(),
    python_requires=">=3.7",
    include_package_data=True,
)
